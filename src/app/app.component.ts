import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { RechargePage } from '../pages/recharge/recharge';
import { AppParams } from './app.module';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { TripHistoryPage } from '../pages/trip-history/trip-history';
import { SignoutPage } from '../pages/signout/signout';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  public name = AppParams.OWNER;


  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

  

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString("58B848");
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  goToDashboard(){
    this.nav.setRoot(DashboardPage);
  }

  doRecharge(){
    this.nav.push(RechargePage);
  }

  signout(){
    this.nav.push(SignoutPage)
  }

  goToPaymentHistory(){
    this.nav.push(PaymentHistoryPage);
  }

  goToTripHistoryPage(){
    this.nav.push(TripHistoryPage);
  }

}
