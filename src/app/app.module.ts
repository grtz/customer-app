import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CreditCardFormatDirective  } from '../../node_modules/angular-cc-library/lib/directives/credit-card-format.directive';
import { ExpiryFormatDirective  } from '../../node_modules/angular-cc-library/lib/directives/expiry-format.directive';
import { CvcFormatDirective   } from '../../node_modules/angular-cc-library/lib/directives/cvc-format.directive';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignupPage } from '../pages/signup/signup';
import { VerificationPage } from '../pages/verification/verification';
import { PaymentMethodPage } from '../pages/payment-method/payment-method';
import { FormsModule } from '@angular/forms';
import { RechargePage } from '../pages/recharge/recharge';
import { RechargeOptionPage } from '../pages/recharge-option/recharge-option';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PaymentHistoryPage } from '../pages/payment-history/payment-history';
import { TripHistoryPage } from '../pages/trip-history/trip-history';
import { SignoutPage } from '../pages/signout/signout';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SignupPage,
    VerificationPage,
    CreditCardFormatDirective,
    ExpiryFormatDirective,
    CvcFormatDirective,
    RechargePage,
    PaymentMethodPage,
    RechargeOptionPage,
    DashboardPage,
    PaymentHistoryPage,
    TripHistoryPage,
    SignoutPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule, 
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SignupPage,
    VerificationPage,
    RechargePage,
    PaymentMethodPage,
    RechargeOptionPage,
    DashboardPage,
    PaymentHistoryPage,
    TripHistoryPage,
    SignoutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

export const AppParams = Object.freeze({
  BASE_PATH : "http://localhost/",
  APP_TITLE : "Automatic Bus Ticketing System",
  OWNER : "Hiruni Fernando"

})