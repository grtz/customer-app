import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppParams } from '../../app/app.module';
import { RechargeOptionPage } from '../recharge-option/recharge-option';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  name: string;
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
    this.title = AppParams.APP_TITLE
    this.name = AppParams.OWNER;
  }

  goToRechargeOption(){
    this.navCtrl.push(RechargeOptionPage);
  }

}
