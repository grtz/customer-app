import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { DashboardPage } from '../dashboard/dashboard';
import { AppParams } from '../../app/app.module';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  title: any;

  constructor(public navCtrl: NavController) {

    this.title = AppParams.APP_TITLE;
  }


  goToDashboard(){
    this.navCtrl.setRoot(DashboardPage,{},{
      animate:true,
      animation : 'forward'
    });
  }

  gpToSignUp(){

    this.navCtrl.push(SignupPage);

  }

}
